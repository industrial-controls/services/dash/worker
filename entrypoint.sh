#!/bin/bash
source $SPARK_INSTALL/bin/activate
 
export PYTHONPATH=$SPARK_INSTALL/lib/python3.7/site-packages:$PYTHONPATH

if [ -z "$KPRINCIPAL" ]
then
    echo "Please provide Kerberos Principal with ENV KPRINCIPAL"
    exit 1
else
    if [[ $KPASSWORD ]]; then
      echo "Authenticating with provided password for principal $KPRINCIPAL"
      echo "${KPASSWORD}" | kinit ${KPRINCIPAL}@CERN.CH 1> /dev/null
    else
      if [ -f /auth/private.keytab ]
      then
        echo "Authenticating with provided keytab for principal $KPRINCIPAL"
        kinit -kt /auth/private.keytab $KPRINCIPAL
      else
        echo "Please provide Kerberos keytab at /auth/private.keytab or password via KPASSWORD environment variable"
        exit 1
      fi
    fi
fi

echo "Now running $@"
exec "$@"
