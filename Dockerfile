ARG FROM=gitlab-registry.cern.ch/acc-co/devops/python/distribution-images/acc-py_el9_openjdk11_gui_ci:pro
FROM $FROM

COPY influxdb.rhel9.repo /etc/yum.repos.d/influxdb.repo

RUN dnf makecache && \
    dnf install -y krb5-workstation krb5-libs git unzip bsdtar tar rsync CERN-CA-certs which influxdb && \
    dnf clean all && \
    curl -o /etc/krb5.conf https://linux.web.cern.ch/docs/krb5.conf

# RUN yum update -y && yum install -y python3 python3-pip python3-virtualenv git unzip bsdtar rsync CERN-CA-certs which influxdb && \
#     yum clean all


# nxcals-bundle/conf/build_packed_venv.sh

# dnf install -y git unzip bsdtar rsync CERN-CA-certs which influxdb
# dnf install -y krb5-workstation krb5-libs
## curl -o /etc/krb5.conf https://linux.web.cern.ch/docs/krb5.conf


ENV SPARK_INSTALL=/opt/nxcals-spark
ENV NXCALS_PACK_ALL_PACKAGES="true"

RUN mkdir -p $SPARK_INSTALL && \
    mkdir -p $SPARK_INSTALL/work && \
    chmod 777 $SPARK_INSTALL/work 

RUN adduser etlworker -m

RUN cd $SPARK_INSTALL && \
    python -m venv . && \
    chown -R etlworker $SPARK_INSTALL 

USER etlworker

RUN cd $SPARK_INSTALL && \
    source $SPARK_INSTALL/bin/activate && \
    pip list && \
    pip install nxcals pandas scipy pyarrow

RUN chmod 755 ${SPARK_INSTALL}/nxcals-bundle/conf/spark-env.sh
RUN export SPARK_HOME="/opt/nxcals-spark/nxcals-bundle" \
    && export SPARK_CONF_DIR="${SPARK_INSTALL}/nxcals-bundle/conf"  \
    && export NXCALS_WORKSPACE_TEMP_DIR="$SPARK_INSTALL/work" \
    && cd $SPARK_INSTALL  \
    && ${SPARK_INSTALL}/nxcals-bundle/conf/spark-env.sh

#    pip3 install --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple nxcals

RUN source $SPARK_INSTALL/bin/activate &&  \
    pip install git+https://gitlab.cern.ch/industrial-controls/services/dash/dash-etl.git@master 


VOLUME /auth

VOLUME $SPARK_INSTALL/work

WORKDIR $SPARK_INSTALL

USER root
COPY entrypoint.sh /
RUN chmod 755 /entrypoint.sh

USER etlworker

ENTRYPOINT ["/entrypoint.sh"]

CMD ["spark-submit","./work/script.py"]
